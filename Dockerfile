FROM alpine:latest

COPY code /

ENTRYPOINT ["/code/app"]